package org.zs.th.salestaxes.api.service;

import java.math.BigDecimal;

import org.zs.th.salestaxes.api.model.ICart;
import org.zs.th.salestaxes.api.model.IItem;

public interface ITaxService {
	
	/**
	 * Calculate sales tax.
	 * @param item the item
	 * @return the big decimal
	 */
	BigDecimal calculateSalesTax(IItem item);
	
	/**
	 * Calculate tax percentage.
	 * @param item the item
	 * @return the big decimal
	 */
	BigDecimal calculateTaxPercentage(IItem item);
	
	/**
	 * Calculate sales tax.
	 * @param cart the cart
	 * @return the big decimal
	 */
	BigDecimal calculateSalesTax(ICart cart);
	
	/**
	 * Calculate total amount.
	 * @param cart the cart
	 * @return the big decimal
	 */
	BigDecimal calculateTotalAmount(ICart cart);
	
}
