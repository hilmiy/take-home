package org.zs.th.salestaxes.api;

import org.zs.th.salestaxes.api.model.IItem;
import org.zs.th.salestaxes.api.model.ItemType;

public interface IItemFactory {
	
	/**
	 * Creates a new IItem object.
	 * @param type the type
	 * @return the i item
	 */
	IItem createItem(ItemType type);
	
	/**
	 * Creates a new IItem object.
	 * @param type the type
	 * @param imported the imported
	 * @param price the price
	 * @param amount the amount
	 * @return the i item
	 */
	IItem createItem(ItemType type, boolean imported, double price, int amount);
}
