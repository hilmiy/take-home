package org.zs.th.salestaxes.api.model;

import java.math.BigDecimal;
import java.util.List;

public interface ICart {
	
	/**
	 * Adds the item.
	 * @param item the item
	 */
	void addItem(IItem item);
	
	/**
	 * Removes the item.
	 * @param item the item
	 */
	void removeItem(IItem item);
	
	/**
	 * Gets the items.
	 * @return the items
	 */
	public List<IItem> getItems();
	
	/**
	 * Gets the sales tax.
	 * @return the sales tax
	 */
	BigDecimal getSalesTax();
	
	/**
	 * Gets the total amount.
	 * @return the total amount
	 */
	BigDecimal getTotalAmount();
	
	/**
	 * Gets the item count.
	 * @return the item count
	 */
	int getItemCount();
	
	/**
	 * Prints the receipt.
	 * @return the string
	 */
	String printReceipt();
	
}
