package org.zs.th.salestaxes.api.model;

import java.util.Locale;

public enum ItemType {
	
	BOOK, FOOD, MEDICAL, OTHER;
	
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return this.name().toLowerCase(Locale.ENGLISH);
	};
	
}
