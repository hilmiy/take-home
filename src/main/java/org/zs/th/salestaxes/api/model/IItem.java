package org.zs.th.salestaxes.api.model;

import java.math.BigDecimal;

public interface IItem {
	
	/**
	 * Gets the price.
	 * @return the price
	 */
	BigDecimal getPrice();
	
	/**
	 * Gets the type.
	 * @return the type
	 */
	ItemType getType();
	
	/**
	 * Sets the price.
	 * @param price the new price
	 */
	void setPrice(BigDecimal price);
	
	/**
	 * Sets the imported.
	 * @param imported the new imported
	 */
	void setImported(boolean imported);
	
	/**
	 * Checks if is imported.
	 * @return true, if is imported
	 */
	boolean isImported();
	
	/**
	 * Sets the amount.
	 * @param amount the new amount
	 */
	void setAmount(int amount);
	
	/**
	 * Gets the amount.
	 * @return the amount
	 */
	int getAmount();
	
	/**
	 * Gets the tax price.
	 * @return the tax price
	 */
	BigDecimal getTaxPrice();
}
