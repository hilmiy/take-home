package org.zs.th.salestaxes.api.service;

import org.zs.th.salestaxes.api.model.ICart;

public interface IPrintService {
	
	/**
	 * Prints the receipt.
	 *
	 * @param cart the cart
	 * @return the string
	 */
	String printReceipt(ICart cart);
	
}
