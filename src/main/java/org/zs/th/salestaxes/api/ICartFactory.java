package org.zs.th.salestaxes.api;

import org.zs.th.salestaxes.api.model.ICart;

public interface ICartFactory {
	
	/**
	 * Creates a new ICart object.
	 * @return the i cart
	 */
	ICart createCart();
}
