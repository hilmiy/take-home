package org.zs.th.salestaxes.impl.model;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.zs.th.salestaxes.api.model.IItem;
import org.zs.th.salestaxes.api.model.ItemType;
import org.zs.th.salestaxes.api.service.ITaxService;

public class Item implements IItem {
	
	private static final Logger	LOGGER	= LoggerFactory.getLogger(Cart.class);
	
	private ItemType			type;
	
	private int					amount;
	
	private boolean				imported;
	
	private BigDecimal			price;
	
	@Autowired
	private ITaxService			taxService;
	
	/**
	 * Instantiates a new item.
	 *
	 * @param type the type
	 */
	public Item(ItemType type) {
		this.type = type;
		this.amount = 1;
	}
	
	/**
	 * Instantiates a new item.
	 *
	 * @param type the type
	 * @param imported the imported
	 * @param price the price
	 * @param amount the amount
	 */
	public Item(ItemType type, boolean imported, BigDecimal price, int amount) {
		this.type = type;
		this.imported = imported;
		this.price = price;
		this.amount = amount;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.IItem#getType()
	 */
	public ItemType getType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.IItem#setPrice(java.math.BigDecimal)
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.IItem#getPrice()
	 */
	public BigDecimal getPrice() {
		return price;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.IItem#setImported(boolean)
	 */
	public void setImported(boolean imported) {
		this.imported = imported;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.IItem#isImported()
	 */
	public boolean isImported() {
		return imported;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.IItem#setAmount(int)
	 */
	@Override
	public void setAmount(int amount) {
		if (amount < 1) {
			LOGGER.error("Amound is less than 1 [{}].", amount);
			throw new IllegalArgumentException("Amount is less than 1!");
		}
		this.amount = amount;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.IItem#getAmount()
	 */
	@Override
	public int getAmount() {
		return amount;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.IItem#getTaxPrice()
	 */
	public BigDecimal getTaxPrice() {
		return taxService.calculateSalesTax(this);
	}
}
