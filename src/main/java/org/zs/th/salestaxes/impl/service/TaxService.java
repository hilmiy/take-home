package org.zs.th.salestaxes.impl.service;

import java.math.BigDecimal;
import java.util.List;

import org.zs.th.salestaxes.api.model.ICart;
import org.zs.th.salestaxes.api.model.IItem;
import org.zs.th.salestaxes.api.model.ItemType;
import org.zs.th.salestaxes.api.service.ITaxService;

public class TaxService implements ITaxService {
	
	private static final BigDecimal	BASIC_TAX_PERCENTAGE	= new BigDecimal("10.00");
	
	private static final BigDecimal	IMPORTED_TAX_PERCENTAGE	= new BigDecimal("5.00");
	
	private static final BigDecimal	ROUNDING_RULE			= new BigDecimal("0.05");
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.service.ITaxService#calculateSalesTax(org.zs.th.salestaxes.api.model.IItem)
	 */
	@Override
	public BigDecimal calculateSalesTax(IItem item) {
		BigDecimal taxPercentage = calculateTaxPercentage(item);
		BigDecimal salesTax = item.getPrice().multiply(taxPercentage).divide(new BigDecimal("100.00"));
		salesTax = round(salesTax);
		return salesTax;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.service.ITaxService#calculateTaxPercentage(org.zs.th.salestaxes.api.model.IItem)
	 */
	@Override
	public BigDecimal calculateTaxPercentage(IItem item) {
		BigDecimal taxPercentage;
		ItemType type = item.getType();
		if (type.equals(ItemType.BOOK) || type.equals(ItemType.FOOD) || type.equals(ItemType.MEDICAL)) {
			taxPercentage = new BigDecimal("0.00");
		} else {
			taxPercentage = BASIC_TAX_PERCENTAGE;
		}
		if (item.isImported()) {
			taxPercentage = taxPercentage.add(IMPORTED_TAX_PERCENTAGE);
		}
		return taxPercentage;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.service.ITaxService#calculateSalesTax(org.zs.th.salestaxes.api.model.ICart)
	 */
	@Override
	public BigDecimal calculateSalesTax(ICart cart) {
		BigDecimal taxAmount = new BigDecimal("0.00");
		List<IItem> items = cart.getItems();
		for (IItem item : items) {
			taxAmount = taxAmount.add(item.getTaxPrice());
		}
		return taxAmount;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.service.ITaxService#calculateTotalAmount(org.zs.th.salestaxes.api.model.ICart)
	 */
	@Override
	public BigDecimal calculateTotalAmount(ICart cart) {
		BigDecimal amount = new BigDecimal("0.00");
		List<IItem> items = cart.getItems();
		for (IItem item : items) {
			amount = amount.add(item.getPrice().add(item.getTaxPrice()));
		}
		return amount;
	}
	
	/**
	 * Rounds the given value.
	 *
	 * @param value the value
	 * @return the big decimal
	 */
	private BigDecimal round(BigDecimal value) {
		value = value.divide(ROUNDING_RULE);
		value = new BigDecimal(Math.ceil(value.doubleValue()));
		value = value.multiply(ROUNDING_RULE);
		return value;
	}
	
}
