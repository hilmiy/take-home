package org.zs.th.salestaxes.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.zs.th.salestaxes.api.ICartFactory;
import org.zs.th.salestaxes.api.model.ICart;
import org.zs.th.salestaxes.impl.model.Cart;

public class CartFactory implements ICartFactory {
	
	@Autowired
	private ApplicationContext	applicationContext;
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.ICartFactory#createCart()
	 */
	public ICart createCart() {
		Cart cart = new Cart();
		applicationContext.getAutowireCapableBeanFactory().autowireBean(cart);
		return cart;
	}
}
