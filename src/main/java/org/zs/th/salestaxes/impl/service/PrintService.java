package org.zs.th.salestaxes.impl.service;

import java.math.RoundingMode;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.zs.th.salestaxes.api.model.ICart;
import org.zs.th.salestaxes.api.model.IItem;
import org.zs.th.salestaxes.api.service.IPrintService;
import org.zs.th.salestaxes.api.service.ITaxService;

public class PrintService implements IPrintService {
	
	private static final char	CHAR_NEW_LINE	= '\n';
	
	@Autowired
	private ITaxService			taxService;
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.service.IPrintService#printReceipt(org.zs.th.salestaxes.api.model.ICart)
	 */
	@Override
	public String printReceipt(ICart cart) {
		StringBuilder sb = new StringBuilder();
		sb.append(CHAR_NEW_LINE);
		List<IItem> items = cart.getItems();
		for (IItem item : items) {
			printItem(item, sb);
			sb.append(CHAR_NEW_LINE);
		}
		sb.append("Sales Taxes : ");
		sb.append(taxService.calculateSalesTax(cart).setScale(2, RoundingMode.HALF_UP));
		sb.append(CHAR_NEW_LINE);
		sb.append("Total       : ");
		sb.append(taxService.calculateTotalAmount(cart).setScale(2, RoundingMode.HALF_UP));
		sb.append(CHAR_NEW_LINE);
		return sb.toString();
	}
	
	/**
	 * Prints the item.
	 *
	 * @param item the item
	 * @param sb the sb
	 */
	private void printItem(IItem item, StringBuilder sb) {
		sb.append(item.getAmount());
		if (item.isImported()) {
			sb.append(" imported ");
		} else {
			sb.append(' ');
		}
		sb.append(item.getType());
		sb.append(" : ");
		sb.append(item.getPrice().add(item.getTaxPrice()).setScale(2, RoundingMode.HALF_UP));
	}
	
}
