package org.zs.th.salestaxes.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.zs.th.salestaxes.api.IItemFactory;
import org.zs.th.salestaxes.api.model.IItem;
import org.zs.th.salestaxes.api.model.ItemType;
import org.zs.th.salestaxes.impl.model.Item;

public class ItemFactory implements IItemFactory {
	
	@Autowired
	private ApplicationContext	applicationContext;
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.IItemFactory#createItem(org.zs.th.salestaxes.api.model.ItemType)
	 */
	public IItem createItem(ItemType type) {
		IItem item = null;
		if (type == ItemType.BOOK) {
			item = new Item(ItemType.BOOK);
		} else if (type == ItemType.MEDICAL) {
			item = new Item(ItemType.MEDICAL);
		} else if (type == ItemType.FOOD) {
			item = new Item(ItemType.FOOD);
		} else {
			item = new Item(ItemType.OTHER);
		}
		applicationContext.getAutowireCapableBeanFactory().autowireBean(item);
		return item;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.IItemFactory#createItem(org.zs.th.salestaxes.api.model.ItemType, boolean, double, int)
	 */
	@Override
	public IItem createItem(ItemType type, boolean imported, double price, int amount) {
		IItem item = createItem(type);
		item.setImported(imported);
		item.setPrice(new BigDecimal(price));
		item.setAmount(amount);
		return item;
	}
}
