package org.zs.th.salestaxes.impl.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zs.th.salestaxes.api.model.ICart;
import org.zs.th.salestaxes.api.model.IItem;
import org.zs.th.salestaxes.api.service.IPrintService;
import org.zs.th.salestaxes.api.service.ITaxService;

@Component
public class Cart implements ICart {
	
	private static final Logger	LOGGER	= LoggerFactory.getLogger(Cart.class);
	
	private final List<IItem>	items;
	
	@Autowired
	private ITaxService			taxService;
	
	@Autowired
	private IPrintService		printService;
	
	/**
	 * Instantiates a new cart.
	 */
	public Cart() {
		items = new ArrayList<IItem>();
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.ICart#addItem(org.zs.th.salestaxes.api.model.IItem)
	 */
	public void addItem(IItem item) {
		items.add(item);
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.ICart#getItems()
	 */
	public List<IItem> getItems() {
		return items;
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.ICart#removeItem(org.zs.th.salestaxes.api.model.IItem)
	 */
	public void removeItem(IItem item) {
		items.remove(item);
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.ICart#getSalesTax()
	 */
	@Override
	public BigDecimal getSalesTax() {
		return taxService.calculateSalesTax(this);
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.ICart#getTotalAmount()
	 */
	@Override
	public BigDecimal getTotalAmount() {
		return taxService.calculateTotalAmount(this);
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.ICart#getItemCount()
	 */
	@Override
	public int getItemCount() {
		return items.size();
	}
	
	/* (non-Javadoc)
	 * @see org.zs.th.salestaxes.api.model.ICart#printReceipt()
	 */
	public String printReceipt() {
		String str = printService.printReceipt(this);
		LOGGER.info(str);
		return str;
	}
}
