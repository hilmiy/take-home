package org.zs.th.salestaxes;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.zs.th.salestaxes.api.ICartFactory;
import org.zs.th.salestaxes.api.IItemFactory;
import org.zs.th.salestaxes.api.model.ICart;
import org.zs.th.salestaxes.api.model.ItemType;


@Controller
public class SalesTaxesApp {
	
	private static final String	SHOPPING_CART	= "SHOPPING_CART";
	
	private static final Logger	LOGGER			= LoggerFactory.getLogger(SalesTaxesApp.class);
	
	@Autowired
	private IItemFactory		itemFactory;
	
	@Autowired
	private ICartFactory		cartFactory;
	
	/**
	 * Begin shopping.
	 *
	 * @return the string
	 */
	@RequestMapping(value = "/beginShopping", method = RequestMethod.POST)
	public @ResponseBody
	String beginShopping() {
		getCart();
		LOGGER.debug("Cart created!");
		return "Cart created!";
	}
	
	/**
	 * Adds the item.
	 *
	 * @param type the type
	 * @param imported the imported
	 * @param price the price
	 * @param amount the amount
	 * @return the string
	 */
	@RequestMapping(value = "/addItem", method = RequestMethod.POST)
	public @ResponseBody
	String addItem(@ModelAttribute("type") ItemType type, 
			       @ModelAttribute("imported") boolean imported, 
			       @ModelAttribute("price") double price, 
			       @ModelAttribute("amount") int amount) {
		getCart().addItem(itemFactory.createItem(type, imported, price, amount));
		LOGGER.debug("Item created!");
		return "Item created!";
	}
	
	/**
	 * Prints the receipt.
	 *
	 * @return the string
	 */
	@RequestMapping(value = "/printReceipt", method = RequestMethod.POST)
	public @ResponseBody
	String printReceipt() {
		String receipt = getCart().printReceipt();
		LOGGER.debug(receipt);
		return receipt;
	}
	
	/**
	 * End shopping.
	 *
	 * @return the string
	 */
	@RequestMapping("/endShopping")
	public String endShopping() {
		LOGGER.debug("Cart removed!");
		getSession().setAttribute(SHOPPING_CART, null);
		return "Cart removed!";
	}
	
	/**
	 * Gets the cart.
	 *
	 * @return the cart
	 */
	private ICart getCart() {
		ICart cart = (ICart) getSession().getAttribute(SHOPPING_CART);
		if (cart == null) {
			cart = cartFactory.createCart();
			getSession().setAttribute(SHOPPING_CART, cart);
		}
		return cart;
	}
	
	/**
	 * Gets the session.
	 *
	 * @return the session
	 */
	private HttpSession getSession() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest().getSession(true);
	}
}
