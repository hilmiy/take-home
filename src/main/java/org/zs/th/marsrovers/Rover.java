package org.zs.th.marsrovers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Rover {
	
	private static final Logger	LOGGER			= LoggerFactory.getLogger(Rover.class);
	
	private static final int	DIRECTION_EAST	= 0;									// 0   degree
	private static final int	DIRECTION_NORTH	= 1;									// 90  degree
	private static final int	DIRECTION_WEST	= 2;									// 180 degree
	private static final int	DIRECTION_SOUTH	= 3;									// 270 degree
																						
	private int					x;
	private int					y;
	private int					direction;
	private int					maxX;
	private int					maxY;
	
	/**
	 * Instantiates a new rover.
	 *
	 * @param x the x
	 * @param y the y
	 * @param direction the direction in 
	 * @param maxX the max x
	 * @param maxY the max y
	 */
	public Rover(int x, int y, char direction, int maxX, int maxY) {
		this.x = x;
		this.y = y;
		this.direction = encodeDirection(direction);
		this.maxX = maxX;
		this.maxY = maxY;
	}
	
	/**
	 * Execute command.
	 *
	 * @param command the command
	 */
	public void executeCommand(CharSequence command) {
		for (int i = 0; i < command.length(); i++) {
			executeCommand(command.charAt(i));
		}
	}
	
	/**
	 * Execute command.
	 *
	 * @param command the command
	 */
	public void executeCommand(char command) {
		if (command == 'R') {
			turnRight();
		} else if (command == 'L') {
			turnLeft();
		} else if (command == 'M') {
			move();
		} else {
			LOGGER.error("Command is incorrect [{}]", command);
			throw new IllegalArgumentException("Command is incorrect!");
		}
	}
	
	/**
	 * Turn right.
	 */
	private void turnRight() {
		direction = (direction - 1 + 4) % 4;
	}
	
	/**
	 * Turn left.
	 */
	private void turnLeft() {
		direction = (direction + 1) % 4;
	}
	
	/**
	 * Move.
	 */
	private void move() {
		x-= ((direction - 1 ) % 2);
		y-= ((direction - 2 ) % 2);
		
		//Check if there is an overflow
		if (x < 0) {
			x = 0;
		}
		if (y < 0) {
			y = 0;
		}
		if (x > maxX) {
			x = maxX;
		}
		if (y > maxY) {
			y = maxY;
		}
	}
	
	/**
	 * Decode direction.
	 *
	 * @param direction the direction
	 * @return the char
	 */
	private char decodeDirection(int direction) {
		if (direction == DIRECTION_NORTH) {
			return 'N';
		} else if (direction == DIRECTION_EAST) {
			return 'E';
		} else if (direction == DIRECTION_SOUTH) {
			return 'S';
		} else if (direction == DIRECTION_WEST) {
			return 'W';
		} else {
			LOGGER.error("Direction is incorrect [{}]", direction);
			throw new IllegalArgumentException("Direction is incorrect!");
		}
	}
	
	/**
	 * Encode direction.
	 *
	 * @param direction the direction
	 * @return the int
	 */
	private int encodeDirection(char direction) {
		if (direction == 'N') {
			return DIRECTION_NORTH;
		} else if (direction == 'E') {
			return DIRECTION_EAST;
		} else if (direction == 'S') {
			return DIRECTION_SOUTH;
		} else if (direction == 'W') {
			return DIRECTION_WEST;
		} else {
			LOGGER.error("Direction is incorrect [{}]", direction);
			throw new IllegalArgumentException("Direction is incorrect!");
		}
	}
	
	/**
	 * Gets the cordinates.
	 *
	 * @return the cordinates
	 */
	public String getCordinates() {
		StringBuilder sb = new StringBuilder();
		sb.append(x);
		sb.append(y);
		sb.append(decodeDirection(direction));
		return sb.toString();
	}
}
