package org.zs.th.salestaxes.functional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:salestaxes/test-context.xml" })
@WebAppConfiguration
public class SalesTaxesFunctionalTest {
	
	private static final Logger		LOGGER	= LoggerFactory.getLogger(SalesTaxesFunctionalTest.class);
	
	@Autowired
	private WebApplicationContext	context;
	
	private MockMvc					mockMvc;
	
	private MockHttpSession			mockSession;
	
	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		this.mockSession = new MockHttpSession(context.getServletContext(), UUID.randomUUID().toString());
	}
	
	/**
	 * Test shopping1.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testShopping1() throws Exception {
		
		MvcResult result = this.mockMvc.perform(post("/beginShopping").session(mockSession)).andExpect(status().isOk()).andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
		result = this.mockMvc.perform(post("/addItem")
				.param("type", "BOOK")								  
				.param("imported", "false")
				.param("price", "12.49")
				.param("amount", "1")
				.session(mockSession))
				.andExpect(status().isOk())
				.andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		result = this.mockMvc.perform(post("/addItem")
				  .param("type", "OTHER")
				  .param("imported", "false")
				  .param("price", "14.99")
				  .param("amount", "1")
				  .session(mockSession))
				  .andExpect(status().isOk())
				  .andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		result = this.mockMvc.perform(post("/addItem")
				.param("type", "FOOD")
				.param("imported", "false")
				.param("price", "0.85")
				.param("amount", "1")
				.session(mockSession))
				.andExpect(status().isOk())
				.andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
		
		result = this.mockMvc.perform(post("/printReceipt").session(mockSession)).andExpect(status().isOk()).andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
		result = this.mockMvc.perform(post("/endShopping").session(mockSession)).andExpect(status().isOk()).andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
	}
	
	/**
	 * Test shopping2.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testShopping2() throws Exception {
		
		MvcResult result = this.mockMvc.perform(post("/beginShopping").session(mockSession)).andExpect(status().isOk()).andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
		result = this.mockMvc.perform(post("/addItem")
				.param("type", "FOOD")								  
				.param("imported", "true")
				.param("price", "10.00")
				.param("amount", "1")
				.session(mockSession))
				.andExpect(status().isOk())
				.andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		result = this.mockMvc.perform(post("/addItem")
				  .param("type", "OTHER")
				  .param("imported", "true")
				  .param("price", "47.50")
				  .param("amount", "1")
				  .session(mockSession))
				  .andExpect(status().isOk())
				  .andReturn();
		LOGGER.info(result.getResponse().getContentAsString());		
		
		result = this.mockMvc.perform(post("/printReceipt").session(mockSession)).andExpect(status().isOk()).andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
		result = this.mockMvc.perform(post("/endShopping").session(mockSession)).andExpect(status().isOk()).andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
	}
	
	/**
	 * Test shopping3.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testShopping3() throws Exception {
		
		MvcResult result = this.mockMvc.perform(post("/beginShopping").session(mockSession)).andExpect(status().isOk()).andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
		result = this.mockMvc.perform(post("/addItem")
				.param("type", "OTHER")								  
				.param("imported", "true")
				.param("price", "27.99")
				.param("amount", "1")
				.session(mockSession))
				.andExpect(status().isOk())
				.andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		result = this.mockMvc.perform(post("/addItem")
				  .param("type", "OTHER")
				  .param("imported", "false")
				  .param("price", "18.99")
				  .param("amount", "1")
				  .session(mockSession))
				  .andExpect(status().isOk())
				  .andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		result = this.mockMvc.perform(post("/addItem")
				.param("type", "MEDICAL")
				.param("imported", "false")
				.param("price", "9.75")
				.param("amount", "1")
				.session(mockSession))
				.andExpect(status().isOk())
				.andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		result = this.mockMvc.perform(post("/addItem")
				.param("type", "FOOD")
				.param("imported", "true")
				.param("price", "11.25")
				.param("amount", "1")
				.session(mockSession))
				.andExpect(status().isOk())
				.andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
		
		result = this.mockMvc.perform(post("/printReceipt").session(mockSession)).andExpect(status().isOk()).andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
		result = this.mockMvc.perform(post("/endShopping").session(mockSession)).andExpect(status().isOk()).andReturn();
		LOGGER.info(result.getResponse().getContentAsString());
		
	}
	
	/**
	 * Destroy.
	 */
	@After
	public void destroy() {
	}
}
