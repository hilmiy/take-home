package org.zs.th.salestaxes.integrity;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zs.th.salestaxes.api.ICartFactory;
import org.zs.th.salestaxes.api.IItemFactory;
import org.zs.th.salestaxes.api.model.ICart;
import org.zs.th.salestaxes.api.model.IItem;
import org.zs.th.salestaxes.api.model.ItemType;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:salestaxes/test-context.xml" })
public class SalesTaxesIntegrityTest {
	
	@Autowired
	private IItemFactory	itemFactory;
	
	@Autowired
	private ICartFactory	cartFactory;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		
	}
	
	/**
	 * Test book tax price.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testBookTaxPrice() throws Exception {
		IItem item = itemFactory.createItem(ItemType.BOOK);
		item.setPrice(new BigDecimal("100.00"));
		assertEquals(0.00d, item.getTaxPrice().doubleValue(), 0d);
		item.setImported(true);
		assertEquals(5.00d, item.getTaxPrice().doubleValue(), 0d);
	}
	
	/**
	 * Tes food tax price.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void tesFoodTaxPrice() throws Exception {
		IItem item = itemFactory.createItem(ItemType.FOOD);
		item.setPrice(new BigDecimal("100.00"));
		assertEquals(0.00d, item.getTaxPrice().doubleValue(), 0d);
		item.setImported(true);
		assertEquals(5.00d, item.getTaxPrice().doubleValue(), 0d);
	}
	
	/**
	 * Test medical tax price.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testMedicalTaxPrice() throws Exception {
		IItem item = itemFactory.createItem(ItemType.MEDICAL);
		item.setPrice(new BigDecimal("100.00"));
		assertEquals(0.00d, item.getTaxPrice().doubleValue(), 0d);
		item.setImported(true);
		assertEquals(5.00d, item.getTaxPrice().doubleValue(), 0d);
	}
	
	/**
	 * Tes other items tax price.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void tesOtherItemsTaxPrice() throws Exception {
		IItem item = itemFactory.createItem(ItemType.OTHER);
		item.setPrice(new BigDecimal("100.00"));
		assertEquals(10.00d, item.getTaxPrice().doubleValue(), 0d);
		item.setImported(true);
		assertEquals(15.00d, item.getTaxPrice().doubleValue(), 0d);
	}
	
	/**
	 * Test cart add items1.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testCartAddItems1() throws Exception {
		ICart cart = cartFactory.createCart();
		cart.addItem(itemFactory.createItem(ItemType.BOOK, false, 12.49d, 1));
		cart.addItem(itemFactory.createItem(ItemType.OTHER, false, 14.99d, 1));
		cart.addItem(itemFactory.createItem(ItemType.FOOD, false, 0.85d, 1));
		
		assertEquals(1.50d, cart.getSalesTax().doubleValue(), 0.001d);
		assertEquals(29.83d, cart.getTotalAmount().doubleValue(), 0.001d);
	}
	
	/**
	 * Test cart add items2.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testCartAddItems2() throws Exception {
		ICart cart = cartFactory.createCart();
		cart.addItem(itemFactory.createItem(ItemType.FOOD, true, 10.00d, 1));
		cart.addItem(itemFactory.createItem(ItemType.OTHER, true, 47.50d, 1));
		
		assertEquals(7.65d, cart.getSalesTax().doubleValue(), 0.001d);
		assertEquals(65.15d, cart.getTotalAmount().doubleValue(), 0.001d);
	}
	
	/**
	 * Test cart add items3.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testCartAddItems3() throws Exception {
		ICart cart = cartFactory.createCart();
		cart.addItem(itemFactory.createItem(ItemType.OTHER, true, 27.99d, 1));
		cart.addItem(itemFactory.createItem(ItemType.OTHER, false, 18.99d, 1));
		cart.addItem(itemFactory.createItem(ItemType.MEDICAL, false, 9.75d, 1));
		cart.addItem(itemFactory.createItem(ItemType.FOOD, true, 11.25d, 1));
		
		assertEquals(6.70d, cart.getSalesTax().doubleValue(), 0.001d);
		assertEquals(74.68d, cart.getTotalAmount().doubleValue(), 0.001d);
	}
	
	/**
	 * Destroy.
	 */
	@After
	public void destroy() {
	}
	
}
