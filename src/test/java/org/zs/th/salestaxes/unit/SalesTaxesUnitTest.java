package org.zs.th.salestaxes.unit;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zs.th.salestaxes.api.ICartFactory;
import org.zs.th.salestaxes.api.IItemFactory;
import org.zs.th.salestaxes.api.model.ICart;
import org.zs.th.salestaxes.api.model.IItem;
import org.zs.th.salestaxes.api.model.ItemType;
import org.zs.th.salestaxes.api.service.ITaxService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations = { "classpath:salestaxes/test-context.xml" })
public class SalesTaxesUnitTest {
	
	@Autowired
	private ITaxService		taxService;
	
	@Autowired
	private IItemFactory	itemFactory;
	
	@Autowired
	private ICartFactory	cartFactory;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
	}
	
	/**
	 * Test tax service for book.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testTaxServiceForBook() throws Exception {
		IItem item = itemFactory.createItem(ItemType.BOOK);
		item.setPrice(new BigDecimal("100.00"));
		assertEquals(0.00d, taxService.calculateTaxPercentage(item).doubleValue(), 0d);
		item.setImported(true);
		assertEquals(5.00d, taxService.calculateTaxPercentage(item).doubleValue(), 0d);
	}
	
	/**
	 * Test tax service for food.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testTaxServiceForFood() throws Exception {
		IItem item = itemFactory.createItem(ItemType.FOOD);
		item.setPrice(new BigDecimal("100.00"));
		assertEquals(0.00d, taxService.calculateTaxPercentage(item).doubleValue(), 0d);
		item.setImported(true);
		assertEquals(5.00d, taxService.calculateTaxPercentage(item).doubleValue(), 0d);
	}
	
	/**
	 * Test tax service for medical.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testTaxServiceForMedical() throws Exception {
		IItem item = itemFactory.createItem(ItemType.MEDICAL);
		item.setPrice(new BigDecimal("100.00"));
		assertEquals(0.00d, taxService.calculateTaxPercentage(item).doubleValue(), 0d);
		item.setImported(true);
		assertEquals(5.00d, taxService.calculateTaxPercentage(item).doubleValue(), 0d);
	}
	
	/**
	 * Test tax service for other items.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testTaxServiceForOtherItems() throws Exception {
		IItem item = itemFactory.createItem(ItemType.OTHER);
		item.setPrice(new BigDecimal("100.00"));
		assertEquals(10.00d, taxService.calculateTaxPercentage(item).doubleValue(), 0d);
		item.setImported(true);
		assertEquals(15.00d, taxService.calculateTaxPercentage(item).doubleValue(), 0d);
	}
	
	/**
	 * Test cart items count.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testCartItemsCount() throws Exception {
		ICart cart = cartFactory.createCart();
		cart.addItem(itemFactory.createItem(ItemType.BOOK));
		cart.addItem(itemFactory.createItem(ItemType.FOOD));
		cart.addItem(itemFactory.createItem(ItemType.MEDICAL));
		cart.addItem(itemFactory.createItem(ItemType.OTHER));
		assertEquals(4, cart.getItemCount());
	}
	
	/**
	 * Test rounding rule1.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testRoundingRule1() throws Exception {
		IItem item = itemFactory.createItem(ItemType.OTHER);
		item.setPrice(new BigDecimal("27.99"));
		item.setImported(true);
		assertEquals(4.2d, taxService.calculateSalesTax(item).doubleValue(), 0d);
	}
	
	/**
	 * Test rounding rule2.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testRoundingRule2() throws Exception {
		IItem item = itemFactory.createItem(ItemType.OTHER);
		item.setPrice(new BigDecimal("18.99"));
		item.setImported(true);
		assertEquals(2.85d, taxService.calculateSalesTax(item).doubleValue(), 0d);
	}
	
	/**
	 * Destroy.
	 */
	@After
	public void destroy() {
	}
	
}
