package org.zs.th.marsrovers;

import org.junit.Test;
import org.zs.th.marsrovers.Rover;
import static org.junit.Assert.assertEquals;

/**
 * The Class RoverTest.
 */
public class RoverTest {
	
	/**
	 * Test command.
	 */
	@Test
	public void testCommand() {
		Rover rover1 = new Rover(1, 2, 'N', 5, 5);
		rover1.executeCommand("LMLMLMLMM");
		assertEquals("13N", rover1.getCordinates());
		
		Rover rover2 = new Rover(3, 3, 'E', 5, 5);
		rover2.executeCommand("MMRMMRMRRM");
		assertEquals("51E", rover2.getCordinates());
	}
	
	/**
	 * Test overflow.
	 */
	@Test
	public void testOverflow() {
		Rover rover = new Rover(0, 0, 'N', 5, 5);
		rover.executeCommand("MMMMMMMMMMMMM");
		assertEquals("05N", rover.getCordinates());
	}
}
